In this section will build and train a small model, that inputs patches
of the 10m spacing Sentinel-2 image, and estimates the class of the
center pixel.
Our CNN inputs the 16x16x4 patches we have previously generated, and generates 
the prediction of an output class among 6 labels ranging from 0 to 5 (see table 
in the [*Terrain truth*](../part_1_dataset/#terrain-truth) section).

The principle of the model training is to use the training dataset for the 
optimization of the network, regarding the minimization of the loss function. 
The validation dataset is used to compute the average loss across one epoch, in 
order to select the best model to export, during the training.

## Let's start

Create a new python code, `part_1_training.py`. We start with importing the 
necessary python modules:

```python
--8<-- "python/part_1_train.py:1:3"
```

And we define a few constants that will be used multiple times:

```python
--8<-- "python/part_1_train.py:6:8"
```

## Tensorflow datasets

We first build training and validation datasets, of which our model will 
consume samples during the training. To do that, we use the 
`otbtf.DatasetFromPatchesImages` class to convert our previously generated 
GeoTiff patches into a `tensorflow.Dataset` object, i.e. something that 
Keras/TensorFlow is able to use.

Also, we need to convert the input and the target in the right format.
While the input can be provided as it is, the target needs to be 
transformed into one hot encoding, required by the cross entropy 
loss operator.
We build a preprocessing function named `dataset_preprocessing()`, 
that inputs one single sample, and returns a dictionary of 
keys/values with keys being the name for inputs and targets, and 
values being the tensors to provide to the model and loss operator.

To avoid doing things twice, one time for the training dataset, and
another for the validation dataset, we build a helper function, named
`create_dataset()` that builds for us the TensorFlow dataset from the
list of geotiff patches files.

```python
--8<-- "python/part_1_train.py:11:27"
```

We can then call this helper to build the datasets for training and
validation:

```python
--8<-- "python/part_1_train.py:78:89"
```

!!! Note

    The tranining samples are shuffled each epoch to better represent the loss 
    gradient. If the order of samples is always the same, the risk is that the 
    gradient descent is stuck in a local minimum while a better solution might 
    exist in the neighborhood. Shuffling the batches of samples is a solution 
    to avoid that.

## Custom operators

We will build our model with convolutions operators and max pooling.

### Convolution

The convolution has a REctified Linear Unit (RELU) as activation function.
It performs convolution with unitary strides. The kernel size and the
depth can be configured with the arguments. The convolution does not
perform any padding and generates only the valid part of the output.

```python
--8<-- "python/part_1_train.py:30:39"
```

### Max pooling

We use a *2x2* max pooling operator, with strides *(2, 2)* by default.

```python
--8<-- "python/part_1_train.py:41:41"
```

## Architecture

To build our model, we can build from scratch building on 
`tf.keras.Model`, but we will see how OTBTF helps a lot with the 
`otbtf.BaseModel` class.

```python
--8<-- "python/part_1_train.py:43:45"
```

### Overview

We build a small convolutional neural network consisting of a 4 trainable
convolutions layers and 2 max pooling layers (not trainable). Our 
model inputs *16x16x4* patches (patches of *16x16* pixels, each pixel 
having *4* components) and produce a *1x1x6* output for the 
pseudo-probability distribution of classes (*estimated*), and a *1x1x1* 
output for the estimated class label (*argmax_layer*)

```mermaid
flowchart TD

i((input)) --> normalization -- 16x16x4 --> c1[conv 5x5 + ReLU]
c1 -- 12x12x16 --> p1[Max Pooling 2x2] -- 6x6x16 --> c2[conv 3x3 + ReLU]
c2 -- 4x4x32 --> p2[Max Pooling 2x2] -- 2x2x32 --> c3[conv 2x2 + ReLU]
c3 -- 1x1x64 --> c4[conv 1x1 + Softmax]
c4 -- 1x1x6 --> loss[Softmax cross entropy] --> optimizer
c4 --> argmax -- 1x1x1 --> p((labels))
tt((Terrain truth)) --> loss

```

### Input normalization

The normalization is implemented in `SimpleCNNModel.normalize_intputs()`.

The model is intended to work on real world images, which have often 
16 bits signed integers as pixel values. The model has to normalize 
these values such as they fit the [0, 1] range before reaching the 
convolutions. To do that, we will use a simple scaling of the image
pixels values.

```python
--8<-- "python/part_1_train.py:43:48"
```

### Network implementation

The actual network is implemented in `SimpleCNNModel.get_output()`.

The normalized input is then processed by a succession of
2D-Convolution + activation function (**conv1**, **conv2**, **feats**) 
and pooling layers (**pool1**, **pool2**).
The features after **feats** are processed by a *1x1* layer of 6 neurons 
(one for each predicted class) with a softmax activation function.
The softmax function normalizes the output such as their sum is equal to 1 
and can be used to represent a categorical distribution, i.e, a probability 
distribution over *n* different possible outcomes. The softmax layer is 
named **softmax_layer** in our model.

$$\sigma(z)_i=\frac{e^{z_i}}{\sum\limits_{k=1}^ne^{z_k}}$$

The estimated class is the index of the neuron (from the last neuron
layer) that output the maximum value. This is performed in processing
the outputs of **softmax_layer** with the `otbtf.layers.Argmax`
operator, which we name **argmax_layer**.

Finally, the `get_output()`

```python
--8<-- "python/part_1_train.py:50:67"
```

## Optimization

We can now train our model!

### Scope

First, we define the scope in which the optimization will take place,
and we instantiate our model.

```python
--8<-- "python/part_1_train.py:91:93"
```

Note that for single CPU you can use 
`tf.distribute.OneDeviceStrategy(device="/cpu:0")` and for one or multiple
GPUs on the same computing node you can use 
`tf.distribute.MirroredStrategy()`. Other strategies exist for distributed
training, e.g. `strategy = tf.distribute.MultiWorkerMirroredStrategy(...)`
but this is out of scone of this tutorial (read mode on the 
[otbtf documentation](https://otbtf.readthedocs.io/en/latest/api_distributed.html))

### Compilation

Then, we specify the **loss** and the **optimizer**:

```python
--8<-- "python/part_1_train.py:94:97"
```

We provide to `loss` a dictionary where the key is the name of the model
output, and the value is the function used to compute the loss.

!!! Warning
    The model output and the training/validation datasets targets names 
    must match so that keras is able to compute the loss.
    
We detail in the following subsections the role of `loss` and `optimizer`.

#### Loss

The goal of the training is to minimize the cross-entropy between the
data distribution (real labels) and the model distribution (estimated
labels). Our loss function is the cross-entropy between the softmax of 
the 6 neurons returned by our model `tgt_key` output, and the training 
dataset labels, one-hot encoded.
The categorical cross-entropy measures the probability error in the 
discrete classification task in which the classes are mutually exclusive 
(each entry is in exactly one class).

$$H(p,q)=-\sum\limits_{x}p(x)log(q(x))$$

The categorical cross-entropy is implemented in keras in
`tf.keras.losses.CategoricalCrossentropy()`.

#### Optimizer

The optimizer performs the minimisation of the loss. This operator is used at 
training time only, it is not used during inference. We use an optimizer that 
employs the [*Adaptive moment estimation*](https://arxiv.org/abs/1412.6980)
method. We use the implementation provided in keras by
`tf.keras.optimizers.Adam()`.

### Summary

After compiling it, we can summarize he model like this:

```python
--8<-- "python/part_1_train.py:98:98"
```

### Fitting

Now we call `fit()`, like any keras model (you can read more on the API
in the [keras documentation](https://keras.io/api/models/model_training_apis/)).
This will run the actual training of the model.

```python
--8<-- "python/part_1_train.py:99:104"
```

Here we use the `tf.keras.callbacks.ModelCheckpoint(params.model_dir)` callback 
that will save for us the best model. The `ds_valid` validation dataset will be 
used after each epoch to check if the loss computed over the validation dataset 
has decreased (if yes, a copy of the model is exported in the 
`params.model_dir` directory). The number of epochs is set with the `epochs` 
argument.

!!! Question

    Create the python script `part_1_train.py`. We want that it can be run in 
    command line. Use the `argparse` module to build a simple parser, so that we 
    can provide the following arguments:

    - `model_dir`: the directory to save the trained model
    - `batch_size`: the batch size
    - `learning_rate`: the learning rate
    - `epochs`: the number of epochs

    ```python
    --8<-- "python/part_1_train.py:70:75"
    ```

    Put the pieces together, and run your code using the following command line:

    ```commandLine
    python part_1_train.py --model_dir /data/models/model1
    ```

<details>
  <summary>Solution</summary>
  ```python
  --8<-- "python/part_1_train.py"
  ```
</details>



