Deep nets can be used efficiently to deal with multiple heterogeneous
sources, for instance in implementing a number of branches tailored for
each data source, that eventually merge together at some point to fuse
the information. For example, it is easy to build a deep neural networks
that can deal simultaneously with time series and one very high
resolution image: while one branch composed of a recurrent neural
network processes the time series (For instance Sentinel-1 or Sentinel-2
images), another branch composed of a CNN processes the high resolution
image (for instance Spot 6/7 images).

This section will present how can be used with any number of images
sources.

## Set the number of sources

The number of sources in applications can be controlled using the
environment variable **OTB_TF_NSOURCES**. If this environment variable
is undefined, the default value for the number of sources is 1. 

In the following, we will use 2 sources from the Sentinel-2 image:

1.  Channels at 20m spacing (Spectral bands 5, 6, 7, 8a, 11 and 12),
    stacked in a single raster
2.  Channels at 10m spacing (Spectral bands 4, 3, 2 and 8), stacked in a
    single raster

The architecture inputs two images at different resolution to derive a
single output at the desired resolution.

### Command line interface

We can change the number of sources from the terminal:

```commandLine
export OTB_TF_NSOURCES=2
```

Then, we can check that the number of sources has been increased to 2
in OTBTF applications.

```commandLine
otbcli_PatchesExtraction -help
```

or
```commandLine
otbcli_TensorflowModelServe -help
```

As you can notice, the applications have now 2 sources, namely
***source1*** and ***source2***, each one with its parameters.

### Python

In python we can either use `os.environ["OTB_TF_NSOURCES"]="2"` then 
use OTB applications through `pyotb` or `otbApplication`, or we can
directly use an otbtf-specific feature of `pyotb` enabling to change
the number of source at runtime with the `n_sources` argument:

```python
app = pyotb.TensorflowModelServe(
  n_sources=2,
  ...
)
```

## An example with Sentinel-2 10m and 20m bands

### Dataset

We will use the **PatchesExtraction** application to create two
patches images, one for the image of bands at 20m spacing, and another
one for the image of bands at 10m spacing. We will sample patches of
size 8x8for the image at 20m spacing, and patches of size
16x16for the image at 10m spacing.

```python title="part_2_sample_extraction_2_sources.py"
--8<-- "python/part_2_sample_extraction_2_sources.py"
```

!!! Note

    We have used a different file name for the label patches and the 
    10m patches, because any patch containing no-data in the 10m or in 
    the 20m image is discarded. So the number of retained samples could 
    be different from the mono-source case.

!!! question

    - Run the sample extraction process over the two sources,
    - Check the generated patches in QGIS

### Model

We propose a CNN with two inputs with different resolution: the 10m
spacing bands of Sentinel-2 (Spectral band 2, 3, 4 and 8) and the 20m
spacing bands of Sentinel-2 (Spectral band 5, 6, 7, 8a, 11 and 12). 
This model processes the two inputs jointly and outputs a land cover
map at the lowest resolution, i.e. 20m spacing. The 20m spacing image
patches have a size of *8x8* and the 10m spacing image a size of
*16x16*. The model outputs a single class value for these input patches.

```mermaid
flowchart TD

x1((input1)) --> n1[normalization] --> c11[conv 3x3 + ReLU]
x2((input2)) --> n2[normalization] --> c12[conv 3x3 + ReLU]

c11 --> c21[conv 3x3 + ReLU]
c21 --> c31[conv 3x3 + ReLU]
c31 --> c41[conv 2x2 + ReLU]

c12 --> c22[conv 3x3 + ReLU]
c22 --> c32[conv 3x3 + ReLU]
c32 --> c42[conv 3x3 + ReLU]
c42 --> c52[conv 3x3 + ReLU]
c52 --> c62[conv 3x3 + ReLU]

c62 --> p[Pooling 2x2]
p --> c72[conv 2x2 + ReLU]

c41 --> Concatenate
c72 --> Concatenate
Concatenate --> c8[conv 1x1 + softmax]
c8 -- 1x1x6 --> argmax -- 1x1x1 --> out((labels))
tt((terrain truth)) --> loss
c8 --> loss[Cross entropy]
loss --> Optimizer
```

Our `DualSrcFCNNModel` model normalizes both 10m and 20m images 
using the  method introduced in the previous section.
We implement the model with two independent branches that
process separately the 10m and the 20m Sentinel-2 images.

```python
--8<-- "python/part_2_train_fcn_2_sources.py:61:102"
```

An important aspect is the consistency of the spatial size and 
dimension of the different features arrays of the model, especially
when we want to use it in a fully-convolutional fashion.
Let summarize the size and the physical spacing of the different 
features involved in our model. In the following, *Size* refers to 
the size of the tensor in the physical dimensions (i.e. *x* and *y*
dimensions), and *Spacing* refers to the physical spacing of the 
tensor also in the physical dimension.

**The 10m branch**

<center>

| Name      | Size (pixels) | Spacing (m) |
| --------- | :-----------: | :---------: |
| input 10m | 16x16         | 10          |
| conv1 10m | 12x12         | 10          |
| conv2 10m | 10x10         | 10          |
| conv3 10m | 8x8           | 10          |
| conv4 10m | 6x6           | 10          |
| conv5 10m | 4x4           | 10          |
| pooling 2x2 10m | 2x2     | 20          |

</center>

Across the successive convolutions, features sizes disminish, but
the physical spacing is preserved.
Then, the final pooling layer is applied, changing the physical size
of the feature map, and also the physical spacing: it downsamples 
the features to 20 meters spacing.

**The 20m branch**

<center>

| Name      | Size (pixels) | Spacing (m) |
| --------- | :-----------: | :---------: |
| input 20m | 8x8           | 20          |
| conv1 20m | 6x6           | 20          |
| conv2 20m | 4x4           | 20          |
| conv3 20m | 2x2           | 20          |

</center>

The 20m spacing branch processes the 20m input with three convolutions
with unitary stride, disminishing the size of the output features to
*2x2* but preserving the same physical spacing.

!!! Note

    This downsampling in the 10m branch is crucial so that the model
    can be applied in fully-convulutional mode, because now we can 
    concatenate the features from the 10m branch with the other features 
    from the 20m branch, since they are homogeneous in term of size and
    in term of physical spacing.

!!! question

    - Copy `part_2_inference_fcn_v2.py` to `part_2_train_fcn_2_sources.py` and 
    edit this new file,
    - Modify the datasets so that samples include 20m spacing bands,
    - Implement the new `DualSrcFCNNModel` model using the provided snippet,
    - Run the training and save the model to a the `/data/models/model2_dualsrc` directory,
    - Compare the metrics on the validation dataset with the metrics of the
    single-source model.

<details>
<Summary>Solution</Summary>
Python code:
```python title="part_2_train_fcn_2_sources.py"
--8<-- "python/part_2_train_fcn_2_sources.py"
```

Command:
```commandLine
python part_2_train_fcn_2_sources.py \
  --model_dir /data/models/model2_dualsrc \
  --log_dir /data/logs/model2_dualsrc
```

</details>

### Inference

In this section, we will generate some maps with this new model!

#### Patch based mode

First, we will run the model in patch-based mode. Generate the output
classification map at the resolution of the 20m spacing image. To do
this, just feed the 20m spacing image to the `source1_il`, which is the
reference source for the output physical spacing.

```python title="part_2_inference_2_sources_pb.py"
--8<-- "python/part_2_inference_2_sources_pb.py"
```

!!! question

    - Run the inference to generate the map
    - Switch `source1` and `source2` such as the first source is the 10m 
    spacing image: the output image will be created with the same spacing.
    - Import the images in QGIS and analyse their spatial resolutions.

<details>
<summary>Solution</summary>
```python title="part_2_inference_2_sources_pb_sf.py"
--8<-- "python/part_2_inference_2_sources_pb_sf.py"
```
</details>

#### Fully convolutional mode

Generate the output classification map at the same resolution as the 20m
spacing image, using the `model_fullyconv` parameter to `True` to enable the
fully convolutional mode of the processing.

```python title="part_2_inference_2_sources_fcn.py"
--8<-- "python/part_2_inference_2_sources_fcn.py"
```

!!! question

    - Run the inference to generate the map
    - Switch `source1` and `source2` such as the first source is the 10m 
    spacing image, you have to provide an output scale factor (parameter 
    `spcscale`) to the **TensorflowModelServe** application, to make 
    consistent the physical spacing of the output. In this example, the
    `spcscale` would be *2*.
    
<details>
<summary>Solution</summary>
```python title="part_2_inference_2_sources_fcn_sf.py"
--8<-- "python/part_2_inference_2_sources_fcn_sf.py"
```
</details>

