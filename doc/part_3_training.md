We keep the same training setup as in the *patch-based classification* section.

!!! question

    Create a file named `part_3_train.py` in which you will implement the 
    following.

## Metrics

As addressed in the previous sections, our metrics are Precision, Recall, and 
F1-Score (that we have already defined in `mymetrics.py`).

```python
--8<-- "python/part_3_train.py:95:106"
```

## Callbacks

The different callbacks to be used during training.

### ModelCheckpoint

We let keras compute the metrics over validation dataset after each epoch,
and we save only the model with the lowest validation loss.

```python
--8<-- "python/part_3_train.py:114:120"
```

### TensorBoard

We keep a trace of metrics for TensorBoard using the keras built-in
`tf.keras.callbacks.TensorBoard` class:

```python
--8<-- "python/part_3_train.py:121:122"
```

This is optional, and if we don't provide the `params.log_dir` (from
the command line argument parser `--ckpt_log` argument), it won't be
used.

### BackupAndRestore

Since the training of our model can be a bit long on CPU, we use a callback to 
save regularly the model weights, so that the training can be interrupted and 
restored at any time (e.g. hardware failure, or process killed). 

!!! Info

    The backup-and-restore callback deletes the checkpoint once the training is 
    completed, i.e. after the specified number of epochs in `fit()` has been 
    performed.

```python
--8<-- "python/part_3_train.py:123:125"
```

This is optional, and if we don't provide the `params.ckpt_dir` (from
the command line argument parser `--ckpt_dir` argument), it won't be
used.

!!! Warning

    When the `--ckpt_dir` argument is provided in the CLI, the training will 
    always used the weights saved in the provided directory. Do not forget to 
    delete the directory when you want to restart a training from scratch after 
    a previously interrupted training.

!!! question

    - Implement the backup-and-restore callback,
    - Check that the callback is working during training in interrupting the 
    process with `ctrl+c` keys, and restarts at the right epoch with the same 
    metrics.

## Final evaluation

Finally, once the model is properly trained (i.e. saved when the selected
metric has reach a new minimum), we compute the metrics over the test 
dataset, so that we have a proper evaluation of its performances on an 
independent dataset.

```python
--8<-- "python/part_3_train.py:127:139"
```

!!! question

    - Append this code after `model.fit(...)`, at the end of the training.

You can now run the training using `--ckpt_dir` to select a directory
to write the checkpoints. Also you can change the `--log_dir` to select 
another directory for the TensorBoard logs. Use `--epochs` to select the
number of epochs. 

```commandline
python part_3_train.py \
  --model_dir /data/models/model3 \
  --log_dir /data/logs/model3 \
  --epochs 50 \
  --ckpt_dir /data/ckpts/model3
```

!!! question

    - Compare the metrics over the validation dataset, the test dataset and the 
    training dataset.

<details>
<Summary>Solution</Summary>
```python title="part_3_train.py"
--8<-- "python/part_3_train.py"
```
</details>

## Dig deeper &#128640;

### Image summary

Use TensorFlow summaries to monitor additional data during the training process.

!!! question

    - Use the `tf.summary.create_file_writer()` to create a summary writer in 
    the same directory as the TensorBoard logs,
    - Use a `tf.summary.image()` to add a new summary showing the softmax 
    output for the class id 1

### Custom callback

One can implement a custom callback using the 
[keras callback API](https://keras.io/api/callbacks/).

!!! question

    - Implement a custom callback that wraps the softmax output image summary. 
    The callback must generate the image summary after each epoch (override 
    `on_epoch_end()`). 
    - Test your callback and check that the image summaries are displayed in 
    TensorBoard.

