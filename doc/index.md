# Introduction

This tutorial follows the [**Deep learning on remote sensing images with 
open-source software**](https://www.routledge.com/Deep-Learning-for-Remote-Sensing-Images-with-Open-Source-Software/Cresson/p/book/9780367518981) book published in 2020.
Time has passed and the involved software have evolved quite a lot, and a major 
update of the contents of this book was needed.
In particular, **TensorFlow** is at the heart of OTBTF and has considerably 
changed between v1 (used at the time when the book was released) and v2 with 
the use of **Keras** as the main API.
On the other hand, **pyotb** is a new python library wrapping OTB in a pythonic 
way, easing considerably the use of OTB in python.
This tutorial focuses on the use of OTBTF with the new TensorFlow-Keras python 
API, and pyotb to process remote sensing images with the OTB applications.
This tutorial skips the explanations provided in the book, and focuses on the 
applied part of the exercises in python and command line.

# Prerequisites

## Latest otbtf docker image

Pull the latest CPU docker image.

```commandLine
docker pull mdl4eo/otbtf
```

Then everything we will see in this tutorial happens inside a running docker container.

## Python packages

- [pyotb](https://github.com/orfeotoolbox/pyotb) is a powerful wrapper that 
  eases considerably the use of OTB with python.
- [pystac-client](https://github.com/stac-utils/pystac-client) will be used to 
  reach STAC catalogs
- [planetary-computer](https://github.com/microsoft/planetary-computer-sdk-for-python) 
  enables to sign the URLs of cloud-optimized-geotiff images hosted in the 
  Microsoft Planetary Computer azure blob storage.

You can install all required packages using `pip` after entering an interactive session in the otbtf docker container:

```commandLine
pip install pyotb pystac-client planetary-computer
```

!!! Note
    At the time of this writing, the tutorial steps have been successfully 
    applied using `mdl4eo/otbtf:4.3.0-cpu` image with `pyotb==2.0.2`

# Workspace

In this tutorial, we consider the `/data` folder as our scratch directory.
