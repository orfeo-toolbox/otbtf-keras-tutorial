This section introduces what we call *patch based classification* for
land cover mapping. This task consists in classifying image patches of
fixed size, and attributing the estimated class to the central pixel of
the patch, to produce a land cover map. This kind of approach is suited
when the available data is sparsely annotated: the only requirement is
the location of patches of which the class is known. Typically, examples
of terrain truth allowing this approach can be GPS coordinates
associated to land cover class, or manually annotated polygons in GIS,
etc. This approach involves deep networks that input a patch of image,
and produce a single value that represents the class of this patch:
hence training this kind of network requires only input image patches,
and associated class values. Once the training of a network is done,
producing land cover maps consist in applying the model on each patches
of a remote sensing image, and storing the value of the estimated class
into the output image. The advantage of this kind of approach is its
suitability to sparsely annotated datasets. Its implementation
subtleties and limitations will be discussed along this section.

First we will see how to collect patches in geospatial images.
Then, we will build and train a simple convolutional neural network with 
otbtf and keras. 
Once this model is trained, we will apply it quickly on a remote sensing
image to generate a landcover map and open the result in QGIS.
After that, we will learn how to customize the training process, e.g. 
how to select the best model, how to add metrics and use tensorboard 
to monitor them.
Then, we will consider an important subset of convolutional neural 
networks: the fully convolutional neural networks.
