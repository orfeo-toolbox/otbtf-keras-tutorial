We will now build a small U-Net based model with 4 downscaling/upscaling
levels.

## Architecture

The following figure presents the architecture of our model.

```mermaid
flowchart TD
tt((terrain truth)) --> loss

x1((pan)) --> n1[normalization] -- 64x64x1 --> c11[Conv 3x3, stride 2, ReLU]
c11 -- 32x32x16 --> c21[Conv 3x3, stride 2, ReLU]

x2((xs)) --> n2[normalization] -- 16x16x4 --> c12[Conv 3x3, stride 1, ReLU]

c21 -- 16x16x32 --> plus1((+))
c12 -- 16x16x32 --> plus1((+))

plus1((+)) --> c3[Conv 3x3, stride 2, ReLU]

c3 -- 8x8x64 --> c4[Conv 3x3, stride 2, ReLU]

c4 -- 4x4x64 --> c1t[Transposed Conv 3x3, stride 2, ReLU]
c1t -- 8x8x64 --> plus2((+))
c3 --> plus2((+))
plus2((+)) --> c2t[Transposed Conv 3x3, stride 2, ReLU]
plus1((+)) --> plus3((+))
plus3((+)) --> c3t[Transposed Conv 3x3, stride 2, ReLU]
c2t -- 16x16x32 --> plus3((+))
c11 --> plus4((+))
c3t --> plus4((+))
plus4((+)) -- 32x32x16 --> c4t[Transposed Conv 3x3, stride 2, Softmax]

c4t -- 64x64xN --> argmax -- 1x1x1 --> out((labels))
c4t --> loss[Cross entropy]
loss --> Optimizer

```
With *N* being the number of classes (4 in our case).

## Implementation

We proceed in the same way that we did in the *patch-based classification*
section.

### Constants

First we define some constants:

```python
--8<-- "python/part_3_train.py:8:11"
```

### Dataset helpers

Then we define a few helpers to help building the datasets:

```python title="A helper to create otbtf dataset from lists of patches"
--8<-- "python/part_3_train.py:14:21"
```

```python title="Dataset preprocessing function"
--8<-- "python/part_3_train.py:23:28"
```

```python title="TensorFlow dataset creation from lists of patches"
--8<-- "python/part_3_train.py:30:36"
```

The datasets can be instantiated using the helpers:

```python title="Datasets instantiation"
--8<-- "python/part_3_train.py:152:169"
```

### Operators

We define a convolution operator with 2 strides as default.
Note that unlike for patch-based classification we use padding here, 
to have output tensors with preserved spatial size. This is to help
in building the network in a straightforward fashion:

```python title="Convolution operator"
--8<-- "python/part_3_train.py:39:48"
```

Same for the transposed convolution, that will upsample the tensors
in the spatial dimensions:

```python title="Transposed convolution operator"
--8<-- "python/part_3_train.py:51:60"
```

### Model

The model is built following the [previously detailed architecture](#architecture):

```python
--8<-- "python/part_3_train.py:63:87"
```

!!! question

    - Create `part_3_train.py` and implement the model,
    - Try to implement the training setup on your own. You can start from a 
    code written in previous section, e.g. `part_2_train_fcn.py`.

!!! note

    The solution is given in the next section!

