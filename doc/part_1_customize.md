In this section, we will see how to customize the model training.

## Metrics

### Built-in metrics

We can add metrics to be computed at each epochs during the training.
First we define a list of metrics to be computed.
In the following example, we will define **precision** and 
**recall** metrics for each class (i.e. from 0 to 5).

```python
--8<-- "python/part_1_train_custom.py:97:101"
```

We then pass the metrics list to the `metrics` argument of `compile(...)`, in
the form of a `dict` where the key is the output for which to compute the 
metrics:

```python
--8<-- "python/part_1_train_custom.py:108:112"
```

When we will run the new code, we will see the metrics displayed on the output:

```commandLine
129/185 [===================>..........] - ETA: 0s 
- loss: 0.7192 
- ...
- val_softmax_layer_precision: 0.9400 
- val_softmax_layer_recall: 0.8174 
- val_softmax_layer_precision_1: 0.8095 
- val_softmax_layer_recall_1: 0.8633 
- val_softmax_layer_precision_2: 0.5635 
- val_softmax_layer_recall_2: 0.2886 
- val_softmax_layer_precision_3: 0.6867 
- val_softmax_layer_recall_3: 0.5337 
- val_softmax_layer_precision_4: 0.9014 
- val_softmax_layer_recall_4: 0.6038 
- val_softmax_layer_precision_5: 0.8496 
- val_softmax_layer_recall_5: 0.6882
```

!!! question

    - Copy `part_1_train.py` to `part_1_train_custom.py`, and add the precision 
    and recall metrics.

### Custom metrics

We can also develop our own metrics. Here is an example with the f-score, that 
consist in the harmonic mean of the precision and recall.

$$FScore=2.\frac{Pre*Rec}{Pre+Rec}$$

We will implement our own `FScore` class, inheriting from 
`tf.keras.metrics.Metric` to compute the F-Score.


```python title="mymetrics.py"
--8<-- "python/mymetrics.py"
```

As you can see, our class implements `update_state()` to update the internal
variables, and `reset_state()` to clear all internal variables. The result 
(i.e. the F-Score) is returned in `result()` as the `self.f1` internal variable.


!!! question

    - Create `mymetrics.py` and copy/past the code for `FScore` provided above,
    - Edit `part_1_train_custom.py` to add the F-Score metrics for each classes.

<details>
<summary>Solution</summary>
Import our new metric class in our main code:

```python
--8<-- "python/part_1_train_custom.py:4:4"
```

And then we can append the metric to the metrics list:

```python
--8<-- "python/part_1_train_custom.py:102:106"
```
</details>

## Callbacks

In keras, a callback is an object that can perform actions at various 
stages of training (e.g. at the start or end of an epoch, before or after 
a single batch, etc).
We will introduce a few useful callbacks in the following sections.

### Model selection

The current code uses the default settings of keras, which is to export
the model every epochs. Now we use the `tf.keras.callbacks.ModelCheckpoint` 
keras callback to save the model only when the loss reaches a new minimum over 
the validation dataset.

```python
--8<-- "python/part_1_train_custom.py:114:120"
```

!!! question

    Use the custom model selection loss reference: for instance, instead of the 
    min loss value, save the model when the **FScore** metric reaches a new
    max for the **class 1**.

### Tensorboard

Tensorboard is very useful to monitor in real time your training.
In this section, we provide a very simple example to show how it can
be used to monitor the metrics during the training. Tensorboard and
TensorFlow summaries can be also used for other objects, like images,
etc.

To add summaries to your training, use an instance of the keras built-in
`tf.keras.callbacks.TensorBoard` callback.

```python
--8<-- "python/part_1_train_custom.py:121:121"
```

### Fit() with callbacks

Callbacks are provided to the `fit()` function using the `callbacks` 
argument, in the form of a list of callbacks.
To use the previously created callbacks, we pass them to `fit()`:

```python
--8<-- "python/part_1_train_custom.py:122:127"
```

### Running tensorboard

To start tensorboard, you can run a second otbtf docker image with 
**port 6006** open, then run the following:

```commandLine
tensorboard --logdir /data/logs/ --bind_all
```

To access the tensorboard, just open http://localhost:6006 in your web browser.

!!! info

    In linux environment you can start tensorboard in command-line with the 
    **port 6006** open, looking in the `/data/logs/` directory for logs, using 
    the following command (replace `$DATA_DIR` with your mount point):
    
    ```commandLine
    docker run -ti -v $DATA_DIR:/data -p 6006:6006 mdl4eo/otbtf:4.3.0-cpu tensorboard --logdir /data/logs/ --bind_all
    ```

![Tensorboard](images/tboard.png)

!!! Note

    If tensorbard is running on another server, just replace **localhost**
    with your server URL or IP address.

!!! question

    Edit `part_1_train_custom.py`:
    
    - Add the model selection callback
    - Add the tensorboard callback
    - Add the command line argument `log_dir` to the parser, to specify the 
    logs folders to store the summaries for tensorboard.
    - Run the code using `/data/logs` as tensorboard summaries folder:
    ```commandLine
    python part_1_train_custom.py --model_dir /data/models/model1 --log_dir /data/logs/model1
    ```
    - Observe the scalar summaries in TensorBoard

<details>
  <summary>Solution</summary>
  ```python
  --8<-- "python/part_1_train_custom.py"
  ```
</details>

## Dig deeper &#128640;

### Early stopping callback

The early stopping callback enables to halt the process when the training stops 
improving. 

!!! question

    - Use a `tf.keras.callbacks.EarlyStopping` instance and append it to 
    the callbacks list ([keras documentation](https://keras.io/api/callbacks/early_stopping/)).
    - Observe the effect of the different parameters using tensorboard to monitor the 
    losses and the moment training stops.

