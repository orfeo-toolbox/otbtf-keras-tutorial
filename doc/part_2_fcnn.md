Fully convolutional network runs faster that patch based, enough to
process the full image in reasonable time. However, pooling decimates
the resolution of the output (the output image spacing is 4
times greater than the input pixel spacing). We can modify a bit our
original model to create a pixel-wise fully convolutional network which
preserves the input image spacing.

We remove all pooling operators, and add convolutional layers with
unitary strides. Our goal is to keep the same receptive field and
expression field as the previously used architecture (this way, we can
reuse our images patches that we have generated previously.

The following figure summarizes our new architecture.

```mermaid
flowchart TD

i((input)) --> normalization -- 16x16x4 --> c1[conv 5x5 + ReLU]
c1 -- 12x12x16 --> c2[conv 3x3 + ReLU]
c2 -- 10x10x16 --> c3[conv 3x3 + ReLU]
c3 -- 8x8x16   --> c4[conv 3x3 + ReLU]
c4 -- 6x6x32   --> c5[conv 3x3 + ReLU]
c5 -- 4x4x32   --> c6[conv 3x3 + ReLU]
c6 -- 2x2x32   --> c7[conv 2x2 + ReLU]
c7 -- 1x1x32   --> c8[conv 1x1 + Softmax]
c8 -- 1x1x6    --> argmax -- 1x1x1 --> p((labels))

```

Note that there is no stride in convolution, preserving the physical 
spacing in spatial dimensions.
Now let's implement our fully-convolutional model:


```python
--8<-- "python/part_2_train_fcn.py:55:81"
```

As there is no stride in convolution, and stride in pooling, our new
model has an output scale factor of 1 and will be able to produce
the classification map at the same physical spacing as the input image.

!!! question

    - Copy `part_2_train.py` to `part_2_train_fcn.py` and implement the model 
    in this new file.

<details>
  <summary>Solution</summary>
  ```python
  --8<-- "python/part_2_train_fcn.py"
  ```
</details>

### Training

We can now train this new model over the same patches as for the simple
CNN.

```commandLine
python part_2_train_fcn.py \
  --model_dir /data/models/model1_fcn \
  --log_dir /data/logs/model1_fcn
```

!!! Note

    You can observe that the new model architecture and model setup is more 
    prone to overfitting compared to the previous one. 

!!! question

    - Use a `keras.callbacks.EarlyStopping` to halt the optimization process 
    when the training stops improving,
    - Test various settings of the callback and observe their effects on 
    tensorbard (remember that you can set the number of epochs using the 
    `--epochs` parameter of the CLI parser).

### Inference

We now want to produce the classification map over the entire image.
We use the following script that performs the inference in fully-convolutional
mode with a unitary scale factor, meaning that the ratio between the
input image pixels spacing and the output image pixel spacing is 1.
The output image is saved to */data/map1_fcn_v2.tif*. The entire input
image is processed.

```python title="part_2_inference_fcn_v2.py"
--8<-- "python/part_2_inference_fcn_v2.py"
```

!!! note

    The **optim** parameter group of **TensorflowModelServe** enables to adjust
    settings for the model execution to reach better performances. To speed-up 
    the process, you can force the application to produce the result using 
    large tiles. In the following, we force the output tiles to large 
    rectangular tiles of 128 pixels height, in order to reduce the computation
    from partially overlapping input areas.
    
    ```python
    infer = pyotb.TensorflowModelServe(
        optim_tilesizex=256,
        optim_tilesizey=256,
        ...
    )
    ```
    Note that you must have enough memory for processing large tiles. If the 
    memory is not enough, TensorFlow will throw warning messages indicating 
    that the efficiency of the computation is compromised. In this case, you 
    can reduce the tile height, e.g. 64 or 128.

!!! question

    - Generate the map for the entire image,
    - Set the `source1_nodata` to `0` and generate the map (change the output
    image filename),
    - Open the first map (without no-data) and the second (with no-data
    specified) and compare the resulting maps.
