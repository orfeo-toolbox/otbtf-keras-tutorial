import pyotb

# Input images
img_10m = "/data/s2_tokyo_10m.tif"
img_20m = "/data/s2_tokyo_20m.tif"

# Input samples locations
pos_a = "/data/pos_a.geojson"
pos_b = "/data/pos_b.geojson"

# Output patches files prefixes
prefix_a = "/data/a_"
prefix_b = "/data/b_"

for vec, prefix in zip([pos_a, pos_b], [prefix_a, prefix_b]):
  pyotb.PatchesExtraction(
    n_sources=2,  # Tells the OTB application to use two sources
    source1_il=img_10m,
    source1_patchsizex=16,
    source1_patchsizey=16,
    source1_out=prefix + "img_10m_2src.tif",
    source1_nodata=0,
    source2_il=img_20m,
    source2_patchsizex=8,
    source2_patchsizey=8,
    source2_out=prefix + "img_20m_2src.tif",
    source2_nodata=0,
    vec=vec,
    field="class",
    outlabels=prefix + "labels_2src.tif"
  )
