import argparse
import pyotb

parser = argparse.ArgumentParser(description="Train a classifier from deep net features")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Generate a map using the Random Forest classifier 
# performing on the features coming from the deep net
infer = pyotb.ImageClassifierFromDeepFeatures(
  source1_il="/data/s2_tokyo_10m.tif",
  source1_rfieldx=16,
  source1_rfieldy=16,
  source1_placeholder="input",
  deepmodel_dir=params.model_dir,
  deepmodel_fullyconv=True,
  output_names="feats",
  model="/data/randomforest_from_deep_model.yaml"
)

infer.write(
  "/data/map_rf_from_feats.tif",
  pixel_type="uint8",
  ext_fname="box=4000:4000:1000:1000"
)
