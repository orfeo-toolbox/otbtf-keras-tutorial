import pyotb
import argparse
from pystac_client import Client
from planetary_computer import sign_inplace

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Retrieve images urls using the STAC API
api = Client.open('https://planetarycomputer.microsoft.com/api/stac/v1')
item_id = "S2A_MSIL2A_20190508T012701_R074_T54SUE_20201006T112414"
item = api.get_collection("sentinel-2-l2a").get_item(item_id)
bands = ["B02", "B03", "B04", "B08"]
urls = [sign_inplace(item.assets[key].href) for key in bands]

# Concatenate
concat = pyotb.ConcatenateImages(il=urls)

# Inference
infer = pyotb.TensorflowModelServe(
  source1_il=concat,
  source1_rfieldx=16,
  source1_rfieldy=16,
  source1_placeholder="input",
  model_dir=params.model_dir,
  output_names="argmax_layer"
)

# Write
infer.write(
  "/data/map1.tif",
  pixel_type="uint8",
  ext_fname="box=4000:4000:1000:1000"
)
