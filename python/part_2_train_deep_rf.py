import argparse
import pyotb

parser = argparse.ArgumentParser(description="Train a classifier from deep net features")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()


pyotb.TrainClassifierFromDeepFeatures(
  source1_il="/data/s2_tokyo_10m.tif",
  source1_rfieldx=16,
  source1_rfieldy=16,
  source1_placeholder="input",
  model_dir=params.model_dir,
  model_fullyconv=True,
  output_names="feats",
  vd="/data/pos_a.geojson",
  valid="/data/pos_b.geojson",
  sample_vfn="class",
  sample_bm=0,
  classifier="rf",
  optim_tilesizex=10980,  # Force the model to be applied on a rectangular
  optim_tilesizey=256,    # region, like expected by the classifier application  out=out,
  ram=4096,
  out="/data/randomforest_from_deep_model.yaml"
)

