import pyotb

# Input image (10m bands)
img_10m = "/data/s2_tokyo_10m.tif"

# Input samples locations
pos_a = "/data/pos_a.geojson"
pos_b = "/data/pos_b.geojson"

# Output patches files prefixes
prefix_a = "/data/a_"
prefix_b = "/data/b_"

for vec, prefix in zip([pos_a, pos_b], [prefix_a, prefix_b]):
  pyotb.PatchesExtraction(
    source1_il=img_10m,
    source1_patchsizex=16,
    source1_patchsizey=16,
    source1_out=prefix + "img_10m.tif",
    source1_nodata=0,
    vec=vec,
    field="class",
    outlabels=prefix + "labels.tif"
  )
