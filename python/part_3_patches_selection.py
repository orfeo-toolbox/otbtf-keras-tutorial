import pyotb

pyotb.PatchesSelection({
    "in": "/data/tt.tif",
    "grid.step": 128,    # patch step, in pixels
    "grid.psize": 64,    # patch size, in pixels
    "strategy": "split",
    "strategy.split.trainprop": 0.80,  # proportions
    "strategy.split.validprop": 0.10,
    "strategy.split.testprop": 0.10,
    "outtrain": "/data/vec_train.geojson",  # output files
    "outvalid": "/data/vec_valid.geojson",
    "outtest": "/data/vec_test.geojson"
})
