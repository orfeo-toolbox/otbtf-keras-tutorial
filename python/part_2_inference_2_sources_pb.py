import pyotb
import argparse

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Generate the classification map, patch-based mode, same 
# resolution as the 20m spacing image
infer = pyotb.TensorflowModelServe(
  n_sources=2,  # Tells the OTB application to use two sources
  source1_il="/data/s2_tokyo_20m.tif",
  source1_rfieldx=8,
  source1_rfieldy=8,
  source1_placeholder="input_20m",
  source2_il="/data/s2_tokyo_10m.tif",
  source2_rfieldx=16,
  source2_rfieldy=16,
  source2_placeholder="input_10m",
  model_dir=params.model_dir,
  output_names="argmax_layer"
)

infer.write(
  "/data/map_fcn_2_sources_pb.tif",
  pixel_type="uint8",
  ext_fname="box=2000:2000:500:500"
)

