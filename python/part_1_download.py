from pystac_client import Client
from planetary_computer import sign_inplace
import pyotb

# Search the product
api = Client.open('https://planetarycomputer.microsoft.com/api/stac/v1')
results = api.search(
  bbox=[139.4848, 35.4485, 139.7333, 35.6606], 
  datetime="2019-05-08/2019-05-08",
  collections=["sentinel-2-l2a"]
)

# Grab the first result (there should be only one!)
res = next(results.items())

# Create two rasters for the 10m and 20m bands
stacks = {
  "/data/s2_tokyo_10m.tif": ["B02", "B03", "B04", "B08"],
  "/data/s2_tokyo_20m.tif": ["B05", "B06", "B07", "B8A", "B11", "B12"],
}
for filename, bands in stacks.items():
  urls = [sign_inplace(res.assets[key].href) for key in bands]
  concat = pyotb.ConcatenateImages(il=urls)
  concat.write(
    filename, 
    ext_fname="gdal:co:COMPRESS=DEFLATE", 
    pixel_type="int16"
  )
