import argparse
import otbtf
import tensorflow as tf
import os
from mymetrics import FScore


class_nb = 6           # number of classes
inp_key = "input"      # model input
tgt_key = "estimated"  # model target


def dataset_preprocessing_fn(sample):
    return {
        inp_key: sample["img"],
        tgt_key: otbtf.ops.one_hot(labels=sample["labels"], nb_classes=class_nb)
    }

def create_dataset(img, labels, batch_size=8):
    otbtf_dataset = otbtf.DatasetFromPatchesImages(
        filenames_dict={"img": img, "labels": labels}
    )
    return otbtf_dataset.get_tf_dataset(
        batch_size=batch_size,
        preprocessing_fn=dataset_preprocessing_fn,
        targets_keys=[tgt_key]
    )


# Training dataset
ds_train = create_dataset(
    ["/data/a_img_10m.tif"],
    ["/data/a_labels.tif"]
)
ds_train = ds_train.shuffle(buffer_size=100)

# Validation dataset
ds_valid = create_dataset(
    ["/data/b_img_10m.tif"],
    ["/data/b_labels.tif"]
)


def conv(inp, depth, kernel_size, name, activation="relu"):
    conv_op = tf.keras.layers.Conv2D(
        filters=depth,
        kernel_size=kernel_size,
        strides=1,
        activation=activation,
        padding="valid",
        name=name
    )
    return conv_op(inp)

class FCNNModel(otbtf.ModelBase):
    """" This is a subclass of `otbtf.ModelBase` to implement a CNN """

    def normalize_inputs(self, inputs):
        """ This function nomalizes the input, scaling values by 0.0001 """
        return {inp_key: tf.cast(inputs[inp_key], tf.float32) * 0.0001}

    def get_outputs(self, normalized_inputs):
        """ This function implements the model """
        inp = normalized_inputs[inp_key]
        net = conv(inp, 16, 5, "conv1")  # 12x12x16
        net = conv(net, 16, 3, "conv2")  # 10x10x16
        net = conv(net, 16, 3, "conv3")  # 8x8x16
        net = conv(net, 32, 3, "conv4")  # 6x6x32
        net = conv(net, 32, 3, "conv5")  # 4x4x32
        net = conv(net, 32, 3, "conv6")  # 2x2x32
        net = conv(net, 32, 2, "feats")  # 1x1x32
        
        # Classifier
        estim = conv(net, class_nb, 1, "softmax_layer", "softmax")
        argmax_op = otbtf.layers.Argmax(name="argmax_layer")
        
        return {
            tgt_key: estim, 
            "estimated_labels": argmax_op(estim),  # additional output: class id
            "deep_net_features": net               # additional output: features
        }


parser = argparse.ArgumentParser(description="Train a CNN model")
parser.add_argument("--model_dir", required=True, help="model directory")
parser.add_argument("--log_dir", required=True, help="model directory")
parser.add_argument("--batch_size", type=int, default=4)
parser.add_argument("--learning_rate", type=float, default=0.0002)
parser.add_argument("--epochs", type=int, default=100)
params = parser.parse_args()
tf.get_logger().setLevel('ERROR')

# Logs directory
log_dir = os.path.join(params.log_dir, "fcn")

strategy = tf.distribute.MirroredStrategy()
with strategy.scope():
    model = FCNNModel(dataset_element_spec=ds_train.element_spec)
    
    metrics = [
        cls(class_id=class_id)
        for class_id in range(class_nb)
        for cls in [tf.keras.metrics.Precision, tf.keras.metrics.Recall]
    ]
    metrics += [
        FScore(class_id=class_id, name=f"fscore_cls{class_id}")
        for class_id in range(class_nb)
    ]

    model.compile(
        loss={tgt_key: tf.keras.losses.CategoricalCrossentropy()},
        optimizer=tf.keras.optimizers.Adam(params.learning_rate),
        metrics={tgt_key: metrics}  # compute the metrics for `tgt_key`
    )
    model.summary()
    
    save_callback = tf.keras.callbacks.ModelCheckpoint(
        params.model_dir,     # new directory name
        save_best_only=True,  # save only the best models
        monitor="val_loss",       # metric or loss to monitor
        mode="min",           # when a new min is reached
        verbose=2             # log something when saving
    )
    tb_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
    model.fit(
        ds_train,
        epochs=params.epochs,
        validation_data=ds_valid,
        callbacks=[save_callback, tb_callback]
    )

