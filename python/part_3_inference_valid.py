import pyotb
import argparse

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Generate the classification map
infer = pyotb.TensorflowModelServe(
  n_sources=2,
  source1_il="/data/pan.tif",
  source1_rfieldx=128,
  source1_rfieldy=128,
  source1_placeholder="input_p",
  source2_il="/data/xs.tif",
  source2_rfieldx=32,
  source2_rfieldy=32,
  source2_placeholder="input_xs",
  model_dir=params.model_dir,
  model_fullyconv=True,
  output_efieldx=64,
  output_efieldy=64,
  output_names="softmax_layer_crop32"
)

infer.write(
  "/data/map_valid.tif",
  ext_fname="box=2000:2000:1000:1000"
)

