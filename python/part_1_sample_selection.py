import pyotb

# Input terrain truth
tt_a = (
  "https://github.com/remicres/otbtf_tutorials_resources/raw/"
  "master/01_patch_based_classification/tokyo_dataset/terrain_truth/"
  "terrain_truth_epsg32654_A.tif"
)
tt_b = (
  "https://github.com/remicres/otbtf_tutorials_resources/raw/"
  "master/01_patch_based_classification/tokyo_dataset/terrain_truth/"
  "terrain_truth_epsg32654_B.tif"
)

# Output samples locations
pos_a = "/data/pos_a.geojson"
pos_b = "/data/pos_b.geojson"

for inref, outvec in zip([tt_a, tt_b], [pos_a, pos_b]):
  pyotb.LabelImageSampleSelection(
    inref=inref, 
    nodata=255, 
    outvec=outvec, 
    strategy="constant", 
    strategy_constant_nb=100
  )

