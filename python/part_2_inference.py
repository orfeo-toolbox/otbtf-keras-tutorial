import pyotb
import argparse

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Create the application
infer = pyotb.TensorflowModelServe(
  source1_il="/data/s2_tokyo_10m.tif",  # input image
  source1_rfieldx=16,                   # receptive field size
  source1_rfieldy=16,
  source1_placeholder="input",          # model input name
  model_dir=params.model_dir,           # model directory
  output_names="argmax_layer"           # model output name
)

# Write
infer.write(
  "/data/map1.tif",                    # output image filename
  pixel_type="uint8",                  # output image encoding
  ext_fname="box=4000:4000:1000:1000"  # subset of the output image
)
