import pyotb

# Output RF classifier
out = "/data/randomforest_model.yaml"

# Train a Random Forest classifier with validation
pyotb.TrainVectorClassifier(
  io_vd="/data/a_pixel_values.gpkg",
  valid_vd="/data/b_pixel_values.gpkg",
  feat=["value_0", "value_1", "value_2", "value_3"],
  cfield="class",
  classifier="rf",
  io_out=out
)

