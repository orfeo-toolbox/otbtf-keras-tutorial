import pyotb
import argparse

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()


infer = pyotb.TensorflowModelServe(
  source1_il="/data/s2_tokyo_10m.tif",
  source1_rfieldx=16,
  source1_rfieldy=16,
  source1_placeholder="input",
  model_dir=params.model_dir,
  output_names="argmax_layer",
  model_fullyconv=True,                 # fully-convolutional mode
  output_spcscale=4                     # output / input spacing ratio
)

infer.write(
  "/data/map1_fcn.tif",
  pixel_type="uint8",
  ext_fname="box=1000:1000:250:250"
)
