import pyotb

vec_train = "/data/vec_train.geojson"
vec_valid = "/data/vec_valid.geojson"
vec_test = "/data/vec_test.geojson"

for vec in [vec_train, vec_valid, vec_test]:
    app_extract = pyotb.PatchesExtraction(
        n_sources=3,  # Tells the OTB application to use three sources
        source1_il="/data/pan.tif",
        source1_patchsizex=64,
        source1_patchsizey=64,
        source1_nodata=0,
        source2_il="/data/xs.tif",
        source2_patchsizex=16,
        source2_patchsizey=16,
        source2_nodata=0,
        source3_il="/data/tt.tif",
        source3_patchsizex=64,
        source3_patchsizey=64,
        vec=vec,
        field="id"
    )
    
    # Create an output filename for pan, xs and labels
    name = vec.replace("vec_", "").replace(".geojson", "")
    out_dict = {
        "source1.out": name + "_p_patches.tif",
        "source2.out": name + "_xs_patches.tif",
        "source3.out": name + "_labels_patches.tif",
    }
    pixel_type = {
        "source1.out": "int16",
        "source2.out": "int16",
        "source3.out": "uint8",
    }
    ext_fname = "gdal:co:COMPRESS=DEFLATE"
    app_extract.write(out_dict, pixel_type=pixel_type, ext_fname=ext_fname)

