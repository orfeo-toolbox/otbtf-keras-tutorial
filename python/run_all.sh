#!/bin/bash
set -e

pip install pyotb pystac-client planetary-computer

#python part_1_download.py
python part_1_sample_selection.py
python part_1_sample_extraction.py
python part_1_train.py --model_dir /data/models/model1 --epochs 2
python part_1_train_custom.py --model_dir /data/models/model1 --epochs 2 --log_dir /data/logs/model1

python part_2_sample_extraction_rf.py
python part_2_train_rf.py
python part_2_inference.py --model_dir /data/models/model1
python part_2_inference_fcn.py --model_dir /data/models/model1
python part_2_train_fcn.py --model_dir /data/models/model1_fcn --log_dir /data/logs/model1_fcn --epochs 2
python part_2_inference_fcn_v2.py --model_dir /data/models/model1_fcn
python part_2_train_deep_rf.py --model_dir /data/models/model1_fcn
python part_2_classify_rf.py --model_dir /data/models/model1_fcn
python part_2_sample_extraction_2_sources.py
python part_2_train_fcn_2_sources.py --model_dir /data/models/model2 --epochs 2 --log_dir /data/logs/model2
python part_2_inference_2_sources_pb.py --model_dir /data/models/model2
python part_2_inference_2_sources_pb_sf.py --model_dir /data/models/model2
python part_2_inference_2_sources_fcn.py --model_dir /data/models/model2
python part_2_inference_2_sources_fcn_sf.py --model_dir /data/models/model2

python part_3_patches_selection.py
python part_3_patches_extraction.py
python part_3_train.py --model_dir /data/models/model3 --log_dir /data/logs/model3 --epochs 2
python part_3_inference_artifacts.py --model_dir /data/models/model3
python part_3_inference_valid.py --model_dir /data/models/model3

python part_4_train.py --model_dir /data/models/model3.h5 --log_dir /data/logs/model3 --ckpt_dir /data/ckpts/model3 --epochs 2


