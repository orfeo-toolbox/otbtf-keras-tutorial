import pyotb

# Input image (10m bands)
img = "/data/s2_tokyo_10m.tif"

# Input samples locations
pos_a = "/data/pos_a.geojson"
pos_b = "/data/pos_b.geojson"

# Output vector files 
out_a = "/data/a_pixel_values.gpkg"
out_b = "/data/b_pixel_values.gpkg"

# Extract the pixels values at samples locations
for vec, out in zip([pos_a, pos_b], [out_a, out_b]):
  pyotb.SampleExtraction({"in": img, "vec": vec, "field": "class", "out": out})

