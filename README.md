# otbtf / keras tutorial


[![Latest Release](https://forgemia.inra.fr/orfeo-toolbox/otbtf-keras-tutorial/-/badges/release.svg)](https://forgemia.inra.fr/orfeo-toolbox/otbtf-keras-tutorial/-/releases) 
[![pipeline status](https://forgemia.inra.fr/orfeo-toolbox/otbtf-keras-tutorial/badges/main/pipeline.svg)](https://forgemia.inra.fr/orfeo-toolbox/otbtf-keras-tutorial/-/commits/main) 

## Overview

This tutorial is an update of the [**Deep learning on remote sensing images 
with open-source software**](https://www.routledge.com/Deep-Learning-for-Remote-Sensing-Images-with-Open-Source-Software/Cresson/p/book/9780367518981) book published in 2020.
Time has passed and the involved software have evolved quite a lot, and a major 
update of the contents of this book was needed.

[Start here!](https://orfeo-toolbox.pages.mia.inra.fr/otbtf-keras-tutorial)

## Contact

Rémi Cresson at INRAE

## License

All content is available under the [Creative commons attribution non commercial 
share alike](https://creativecommons.org/licenses/by-nc-sa/4.0/) 
(CC-BY-NC-SA) license.

<center>
<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" alt="license" width="200"/>
</center>

