import pyotb
from pystac_client import Client
from planetary_computer import sign_inplace

img_p = "/data/pan.tif"
img_xs = "/data/xs.tif"

vec_train = "/data/vec_train_sr.geojson"
vec_valid = "/data/vec_valid_sr.geojson"
vec_test = "/data/vec_test_sr.geojson"

# Retrieve products with stac
api = Client.open('https://planetarycomputer.microsoft.com/api/stac/v1')
results = api.search(
    collections=["sentinel-2-l2a"],
    datetime="2022-06-15/2022-08-15",
    bbox=[2.320261,48.855094,2.340860,48.869549],
    filter={"op": "eq", "args": [{"property": "eo:cloud_cover"}, 0]}
)
item = next(results.items())  # first result
bands = ["B04", "B03", "B02", "B08"]  # Note: R, V, B, NIR
urls = [sign_inplace(item.assets[key].href) for key in bands]

# Patches selection
pyotb.PatchesSelection(
    img_xs,
    grid_step=128,    # patch step, in pixels
    grid_psize=64,    # patch size, in pixels
    strategy="split",
    strategy_split_trainprop=0.80,  # proportions
    strategy_split_validprop=0.10,
    strategy_split_testprop=0.10,
    outtrain=vec_train,  # output files
    outvalid=vec_valid,
    outtest=vec_test
)

# Pan-sharpening
pansharp = pyotb.BundleToPerfectSensor(inp=img_p, inxs=img_xs, method="bayes")

# Sentinel-2 bands concatenation
concat = pyotb.ConcatenateImages(il=urls)
img_s2 = pyotb.Superimpose(inr=img_xs, inm=concat)

# Patches extraction
for vec in [vec_train, vec_valid, vec_test]:
    app_extract = pyotb.PatchesExtraction(
        n_sources=2,
        source1_il=pansharp,
        source1_patchsizex=128,
        source1_patchsizey=128,
        source1_nodata=0,
        source2_il=img_s2,
        source2_patchsizex=32,
        source2_patchsizey=32,
        source2_nodata=0,
        vec=vec,
        field="id"
    )
    
    name = vec.replace("vec_", "").replace(".geojson", "")
    out_dict = {
        "source1.out": name + "_hr_patches.tif",
        "source2.out": name + "_lr_patches.tif"
    }
    pixel_type = {
        "source1.out": "int16",
        "source2.out": "int16"
    }
    ext_fname = "gdal:co:COMPRESS=DEFLATE"
    app_extract.write(out_dict, pixel_type=pixel_type, ext_fname=ext_fname)

