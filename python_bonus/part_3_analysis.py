import pyotb
import argparse

parser = argparse.ArgumentParser(description="Apply the model")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()



def infer(margin=None, rfield=256):

  rfield_xs = int(rfield / 4)
  efield = int(rfield - 2 * margin) if margin else rfield

  softmax = pyotb.TensorflowModelServe(
    n_sources=2,
    source1_il="/data/pan.tif",
    source1_rfieldx=rfield,
    source1_rfieldy=rfield,
    source1_placeholder="input_p",
    source2_il="/data/xs.tif",
    source2_rfieldx=rfield_xs,
    source2_rfieldy=rfield_xs,
    source2_placeholder="input_xs",
    model_dir=params.model_dir,
    model_fullyconv=True,
    output_efieldx=efield,
    output_efieldy=efield,
    output_names=f"softmax_layer_crop{margin}" if margin else "softmax_layer"
  )
  return softmax

def compare(ref, img):
  # Extract an ROI fitting the reference image
  roi = pyotb.ExtractROI(
    img,
    mode="fit",
    mode_fit_im=ref
  )
  # Compute the squared error between the ref and img
  se = pyotb.BandMathX(
    il=[roi, ref],
    exp=".25*(im1-im2)*(im1-im2)'"
  )
  # Mean squared errror value
  stats = pyotb.ComputeImagesStatistics(se)
  return stats["out.mean"][0] ** 0.5
  
print("Margin\tMSE")
print("------\t------")
ref = infer(96)[2000:3000, 2000:3000, :]
margins = [8, 16, 24, 32, 48, 64, 80, 96]
mses = {
  margin: compare(ref, infer(margin))
  for margin in margins
}
for margin, mse in mses.items():
  print(f"{margin}\t{mse}")
