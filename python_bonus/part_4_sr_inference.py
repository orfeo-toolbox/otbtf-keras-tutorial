import argparse
import pyotb
from pystac_client import Client
from planetary_computer import sign_inplace

parser = argparse.ArgumentParser(description="SR Inference")
parser.add_argument("--model_dir", required=True, help="model directory")
params = parser.parse_args()

# Retrieve products with stac
api = Client.open('https://planetarycomputer.microsoft.com/api/stac/v1')
results = api.search(
    collections=["sentinel-2-l2a"],
    datetime="2022-06-15/2022-08-15",
    bbox=[2.320261,48.855094,2.340860,48.869549],
    filter={"op": "eq", "args": [{"property": "eo:cloud_cover"}, 0]}
)
item = next(results.items())  # first result
bands = ["B04", "B03", "B02", "B08"]  # Note: [R, V, B, NIR] to match Spot-7
urls = [sign_inplace(item.assets[key].href) for key in bands]

# Sentinel-2 bands concatenation
concat = pyotb.ConcatenateImages(il=urls)

# Resampling to 6.0m spacing
img_s2 = pyotb.OrthoRectification(
    concat,
    outputs_spacingx=6.0,
    outputs_spacingy=-6.0
)

# Inference
margin = 64
rfield = 128
efield = rfield * 4 - 2 * margin
infer = pyotb.TensorflowModelServe(
    source1_il=img_s2,
    source1_rfieldx=rfield,
    source1_rfieldy=rfield,
    source1_placeholder="lr",
    model_dir=params.model_dir,
    model_fullyconv=True,
    output_efieldx=efield,
    output_efieldy=efield,
    output_names=f"tf.cast_1_crop{margin}",  # we use our int16 output
    output_spcscale=0.25
)

infer.write("/data/sr.tif", ext_fname="box=1024:1024:512:512", pixel_type="int16")
