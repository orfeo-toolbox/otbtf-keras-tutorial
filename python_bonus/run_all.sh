#!/bin/bash
set -e

pip install pyotb pystac-client planetary-computer

python part_3_train_deeper.py --model_dir /data/models/model3_deeper --log_dir /data/logs/model3_deeper --ckpt_dir /data/ckpts/model3_deeper --epochs 2
python part_3_analysis.py --model_dir /data/models/model3_deeper

python part_4_sr_dataset.py
python part_4_sr_train.py --model_dir /data/models/sr --log_dir /data/logs/model4_sr --ckpt_dir /data/ckpts/model4_sr --epochs 2
python part_4_sr_inference.py --model_dir /data/models/sr
