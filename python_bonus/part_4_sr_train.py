import otbtf
import tensorflow as tf
import argparse


inp_key = "lr"
tgt_key = "hr"

norm_scale = 1e-4  # used to normalize the images

def to_float(x):
    return tf.cast(x, tf.float32)


def dataset_preprocessing_fn(sample):
    """ Normalize the target image (the input is normalized in the model)"""
    return {
        inp_key: to_float(sample[inp_key]),
        tgt_key: norm_scale * to_float(sample[tgt_key])
    }


def create_dataset(lr, hr, batch_size=4):
    """Create a dataset that normalizes the target image"""
    otbtf_dataset = otbtf.DatasetFromPatchesImages(
        filenames_dict={"lr": lr, "hr": hr}
    )
    return otbtf_dataset.get_tf_dataset(
        batch_size=batch_size,
        targets_keys=[tgt_key],
        preprocessing_fn=dataset_preprocessing_fn
    )


def conv(inp, depth, name, kernel_size=3, activation="relu"):
    conv_op = tf.keras.layers.Conv2D(
        filters=depth,
        kernel_size=kernel_size,
        strides=1,
        activation=activation,
        padding="same",
        name=name
    )
    return conv_op(inp)


def tconv(inp, depth, name, activation="relu"):
    tconv_op = tf.keras.layers.Conv2DTranspose(
        filters=depth,
        kernel_size=3,
        strides=2,
        activation=activation,
        padding="same",
        name=name
    )
    return tconv_op(inp)


relu = tf.keras.layers.ReLU()


class SRModel(otbtf.ModelBase):
    """Small SR model following the architecture of Ledig et al."""

    def normalize_inputs(self, inputs):
        return {inp_key: norm_scale * to_float(inputs[inp_key])}

    def get_outputs(self, normalized_inputs):

        def resblock(x, name):
            conv1 = conv(x, 64, f"{name}_conv1")
            conv2 = conv(conv1, 64, f"{name}_conv2", activation=None)
            return relu(x + conv2)

        inp = normalized_inputs[inp_key]
        
        # First conv
        conv1 = conv(inp, 64, "conv1", kernel_size=7)
        
        # Residual blocks
        net = conv1
        for i in range(8):
            net = resblock(net, f"resblock{i}")
        net = net + conv1
        
        # Upsampling
        up1 = tconv(net, 128, "conv1t")
        up2 = tconv(up1, 256, "conv2t")
        
        # Last layer
        out = conv(up2, 4, "last_conv", kernel_size=7)
        
        # Last layer (unscaled, int16)
        inv_norm_scale = 1.0 / norm_scale
        extra_out = tf.cast(inv_norm_scale * out, tf.int16)

        return {tgt_key: out, "unscaled_out": extra_out}


def train(params, ds_train, ds_valid, ds_test):
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        model = SRModel(dataset_element_spec=ds_train.element_spec)
        metrics = [
            tf.keras.metrics.MeanAbsoluteError(),
            tf.keras.metrics.RootMeanSquaredError()
        ]

        model.compile(
            loss={tgt_key: "mean_absolute_error"},
            optimizer=tf.keras.optimizers.Adam(params.learning_rate),
            metrics={tgt_key: metrics}
        )
        model.summary()
        save_best_cb = tf.keras.callbacks.ModelCheckpoint(
            params.model_dir,
            mode="min",
            save_best_only=True,
            monitor="val_loss"
        )
        callbacks = [save_best_cb]
        if params.log_dir:
            callbacks.append(tf.keras.callbacks.TensorBoard(log_dir=params.log_dir))
        if params.ckpt_dir:
            ckpt_cb = tf.keras.callbacks.BackupAndRestore(backup_dir=params.ckpt_dir)
            callbacks.append(ckpt_cb)

        model.fit(
            ds_train,
            epochs=params.epochs,
            validation_data=ds_valid,
            callbacks=callbacks
        )

        # Evaluation on the test dataset
        model.load_weights(params.model_dir)
        values = model.evaluate(ds_test, batch_size=params.batch_size)
        for metric_name, value in zip(model.metrics_names, values):
            print(f"{metric_name}: {value}")


parser = argparse.ArgumentParser(description="Train a FCNN model")
parser.add_argument("--model_dir", required=True, help="model directory")
parser.add_argument("--log_dir", help="log directory")
parser.add_argument("--batch_size", type=int, default=4)
parser.add_argument("--learning_rate", type=float, default=0.0002)
parser.add_argument("--epochs", type=int, default=100)
parser.add_argument("--ckpt_dir", help="Directory for checkpoints")
params = parser.parse_args()
tf.get_logger().setLevel('ERROR')

ds_train = create_dataset(
    ["/data/train_sr_lr_patches.tif"],
    ["/data/train_sr_hr_patches.tif"]
)
ds_train = ds_train.shuffle(buffer_size=100)

ds_valid = create_dataset(
    ["/data/valid_sr_lr_patches.tif"],
    ["/data/valid_sr_hr_patches.tif"]
)

ds_test = create_dataset(
    ["/data/test_sr_lr_patches.tif"],
    ["/data/test_sr_hr_patches.tif"]
)

train(params, ds_train, ds_valid, ds_test)
